#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "WXApiManager.h"
#import "WXApiRequestHandler.h"
#import "WXApiResponseHandler.h"
#import "Constant.h"
#import "GetMessageFromWXResp+responseWithTextOrMediaMessage.h"
#import "SendMessageToWXReq+requestWithTextOrMediaMessage.h"
#import "UIAlertView+WX.h"
#import "WXMediaMessage+messageConstruct.h"
#import "WechatAuthSDK.h"
#import "WXApi.h"
#import "WXApiObject.h"

FOUNDATION_EXPORT double ocappVersionNumber;
FOUNDATION_EXPORT const unsigned char ocappVersionString[];


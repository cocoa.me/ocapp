#
# Be sure to run `pod lib lint ocapp.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ocapp'
  s.version          = '0.1.0'
  s.summary          = 'A short description of ocapp.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/zhuxietong/ocapp'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'zhuxietong' => 'zhuxietong@me.com' }
  s.source           = { :git => 'https://gitee.com/cocoa.me/ocapp.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  #s.source_files = 'ocapp/Classes/**/*'
  

  


    s.subspec 'AliPay' do |app|
        app.name = 'AliPay'
        app.source_files = [
        'ocapp/Classes/AliPay/*.swift',
        ]
        
        app.vendored_frameworks = 'ocapp/Classes/AliPay/SDK/AlipaySDK.framework'
        app.ios.frameworks = 'CFNetwork','SystemConfiguration','CoreGraphics','CoreTelephony','CoreMotion','CoreText'
        app.ios.libraries = 'stdc++','c++'
        app.resources  = ["ocapp/Classes/AliPay/SDK/AlipaySDK.bundle"]
    end

     s.subspec 'QQ' do |app|
         app.name = 'QQ'

         app.vendored_frameworks = 'ocapp/Classes/QQ/SDK/TencentOpenAPI.framework'
         app.ios.frameworks = 'Security','SystemConfiguration', 'CoreGraphics','CoreTelephony'
         app.ios.libraries = 'iconv','sqlite3','stdc++','z'
     end

    s.subspec 'WeChat' do |app|
        
        app.name = 'WeChat'
        app.dependency 'WechatOpenSDK'
    end

    s.subspec 'Sina' do |app|
        app.name = 'Sina'
        app.source_files = [
        'ocapp/Classes/Sina/SDK/*.{h}',
        ]

        #sina.pod_target_xcconfig = { 'OTHER_LDFLAGS' => '"-ObjC"' }
        app.vendored_libraries  = 'ocapp/Classes/Sina/SDK/libWeiboSDK.a'
        app.ios.frameworks   = 'Photos', 'ImageIO', 'SystemConfiguration', 'CoreText', 'QuartzCore', 'Security', 'UIKit', 'Foundation', 'CoreGraphics','CoreTelephony'
        app.ios.libraries = 'sqlite3', 'z'
        app.resources  = ["ocapp/Classes/Sina/SDK/WeiboSDK.bundle"]
        
    end

 
    s.default_subspec = 'AliPay','QQ','WeChat','Sina'

 

  
  # s.resource_bundles = {
  #   'ocapp' => ['ocapp/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end

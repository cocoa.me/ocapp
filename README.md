# ocapp

[![CI Status](https://img.shields.io/travis/zhuxietong/ocapp.svg?style=flat)](https://travis-ci.org/zhuxietong/ocapp)
[![Version](https://img.shields.io/cocoapods/v/ocapp.svg?style=flat)](https://cocoapods.org/pods/ocapp)
[![License](https://img.shields.io/cocoapods/l/ocapp.svg?style=flat)](https://cocoapods.org/pods/ocapp)
[![Platform](https://img.shields.io/cocoapods/p/ocapp.svg?style=flat)](https://cocoapods.org/pods/ocapp)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ocapp is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ocapp'
```

## Author

zhuxietong, zhuxietong@me.com

## License

ocapp is available under the MIT license. See the LICENSE file for more info.

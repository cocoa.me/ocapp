//
//  NSArray+mutable.h
//  XMLConvert
//
//  Created by WangLei on 15/7/19.
//  Copyright (c) 2015年 zhuxietong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MEMutableFather.h"
@interface NSArray (mutable)

- (NSMutableArray*)toMutableArray;

@end

//
//  MEMutableFather.h
//  TZMHTeacher
//
//  Created by 朱撷潼 on 14/10/22.
//  Copyright (c) 2014年 zhuxietong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MEMutableFather : NSObject

- (NSMutableDictionary*)convertToMutableDictionary:(NSDictionary*)dict;

- (NSMutableArray*)convertToMutableArray:(NSArray*)array;


+ (MEMutableFather*)defaultCenter;

+ (NSMutableDictionary*)convertDictionary:(NSDictionary*)dict;
+ (NSMutableArray*)convertArray:(NSArray*)array;

@end

//
//  MEShareCenter.h
//  培训通
//
//  Created by 朱撷潼 on 14/11/14.
//  Copyright (c) 2014年 zhuxietong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShareManager.h"

//Bundle(for: JP预订单确认Ctr.self).path(forResource: "joplane", ofType: "bundle")
@class MEJsOCMethods;

typedef NS_ENUM(NSUInteger, MESharePaltformAPP) {
    MESharePaltformQQ,
    MESharePaltformWX,
    MESharePaltformWB
};

@protocol MEShareCenterDelegate <NSObject>

- (void)tellShareResultInfo:(NSMutableDictionary*)result;

@end

@interface MEShareCenter : NSObject
@property (nonatomic,strong) UIImageView *tempImageV;
@property (nonatomic,strong) ShareManager *shareManager;

@property (nonatomic,strong) NSString *shareCallBackID;

@property (nonatomic,strong) NSMutableDictionary *sharePaltform;

@property (nonatomic,strong) NSMutableDictionary *shareWebInfo;

@property (nonatomic,strong) MEJsOCMethods *methodCreater;

@property (nonatomic,strong) UIImage * defaultImg;



@property (nonatomic,weak) id <MEShareCenterDelegate>delegate;

+ (MEShareCenter*)defaultCenter;
    
+ (NSBundle*)shareBundle;
    
+ (void)setShareBundle:(NSBundle*)bundle;

+ (UIImage*)imageWith:(NSString*)name;

- (void)shareWebInfoWithPlatformIndex:(NSInteger)index;

- (void)clearShareInfo;

- (void)loadImageUrls:(NSMutableArray*)array;


- (void)tellShareResult:(NSMutableDictionary*)result;


- (BOOL)installedShareApp:(MESharePaltformAPP)paltformAPP;


@end

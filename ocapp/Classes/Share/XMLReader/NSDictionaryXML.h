//
//  NSDictionaryXML.h
//  Yogo
//
//  Created by WangLei on 15/7/19.
//  Copyright (c) 2015年 zhuxietong. All rights reserved.
//

#ifndef Yogo_NSDictionaryXML_h
#define Yogo_NSDictionaryXML_h
#import "NSArray+mutable.h"
#import "NSDictionary+mutable.h"
#import "XMLReader.h"

#endif

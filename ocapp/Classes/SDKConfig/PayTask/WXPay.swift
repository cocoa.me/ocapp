//
//  WXPay.swift
//  Aiwei
//
//  Created by zhu xietong on 2017/12/11.
//  Copyright © 2017年 zhu xietong. All rights reserved.
//

import Foundation
import Eelay




enum WXPayResult:Int
{
    case ok = 0
    case canle = -2
    case failed = -1
    case notice_failed = 4
    
}

enum PayTaskType:String{
    
    static var Key:String = "WX_PayTaskType"
    
    case goods = "goods"
    case book = "book"

    
    case health = "health"
    
    case none = ""
    
    
    var ID:String{
        get{
            return "PayTaskType_\(self.rawValue)"
        }
    }
    

}


class FastWXPay: NSObject {
    
    struct Static {
        static var instance:FastWXPay? = nil
        static var token:Int = 0
    }
    
    @objc static var shared:FastWXPay{
        if Static.instance == nil{
            Static.instance = FastWXPay()
        }
        return Static.instance!
    }
    
    required override init() {
        super.init()
    }
    
    
    
    var finish_block:((WXPayResult,PayTaskType,String)-> Void) = {_,_,_ in}
    var checkTime:Int = 0
    
    @objc func payCallBack(info:NSMutableDictionary) {
        var taskType:PayTaskType = .none

        if let type_s = UserDefaults.standard.object(forKey: PayTaskType.Key) as? String
        {
            taskType = PayTaskType(rawValue: type_s) ?? .none
        }
        
        if info["code","-100"] == "0"{
            
        }
        
        self.checkTime += 1
        if info["code","-100"] == "0"
        {

            if let _ = UserDefaults.standard.object(forKey: PayTaskType.Key)
            {
                self.finish_block(.ok,taskType,"支付成功")
            }
            return
        }
        else if info["resultStatus","-100"] == "-2"
        {
            self.finish_block(.canle,taskType,"支付取消")
            return
        }
        print("===000|\(#line)")

        self.finish_block(WXPayResult.failed,taskType,"支付失败")
        
    }
    
    
    
    func pay(_ info:NSMutableDictionary,task_type:PayTaskType,finish_block:@escaping (WXPayResult,PayTaskType,String)-> Void) {
        
        debugPrint("wx_pay|\(info)")
        UserDefaults.standard.set(task_type.ID, forKey: PayTaskType.Key)
        
        self.checkTime = 0
        self.finish_block = finish_block
        
        
        let time = info["timestamp","0"]
//        time = time.subString(start: 0, length: 10)
        
        let i_time = Int(time)!
        
        let partnerId = info["partnerid",""]
        let prepayId = info["prepayid",""]
        let sign = info["sign",""]
        let nonceStr = info["noncestr",""]
        //        let timeStamp = info["timestamp",""]
        let package = info["package",""]
        
        let req = PayReq()
        req.partnerId = partnerId
        req.prepayId = prepayId
        req.nonceStr = nonceStr
        req.timeStamp = UInt32(i_time)
        req.package = package
        req.sign = sign
        WXApi.send(req)
        
        //
        debugPrint("partnerId|\(partnerId)----prepayId|\(sign)-----timeStamp|\(i_time)----nonceStr|\(nonceStr)----prepayId|\(prepayId)----package|\(package)")
    }
    
    
}

//
//  SDKConfig.swift
//  Aiwei
//
//  Created by zhu xietong on 2017/12/5.
//  Copyright © 2017年 zhu xietong. All rights reserved.
//

import Foundation
extension Data {
    //将Data转换为String
    var hexString: String {
        return withUnsafeBytes {(bytes: UnsafePointer<UInt8>) -> String in
            let buffer = UnsafeBufferPointer(start: bytes, count: count)
            return buffer.map {String(format: "%02hhx", $0)}.reduce("", { $0 + $1 })
        }
    }
}

public enum PopularSDK {
    case QQ
    case WX
    case Sina
    case Alibaba
}

extension JoTask {
    @discardableResult
    func wxurl(_ url:WXURL) ->JoTask {
        self.url_value = url.url
        return self
    }
}


enum WXURL:String{
    
    case accessToken = "sns/oauth2/access_token"
    case userinfo = "sns/userinfo"
   
    
    static var dist_domian:String = "https://api.weixin.qq.com/"
    
    var url:String{
        get{
            
            return WXURL.dist_domian + self.rawValue
        }
    }
    
}



public struct SDKConfig {
    
    public struct AliPush
    {
        public static var appKey = "24768061"
        public static var secret = "ccbf366519b24b6f68418acc4d5a8a85"
    }
    
    
    public struct JPush
    {
        public static var appKey = "77f5957eb2c4fec55365ea5d"
        public static var secret = "cd1c840aed8986e681e423f0"
    }
    
    public struct QQ
    {
        public static var appKey = "xqAzHUrgt5r6Fs4Q"
        public static var appID = "1106509460"
    }
    
    public struct WX{
        public static var appID = "wxf8456ee79d84c393" //wxcf3ffbb62fe99c06 //wxf8456ee79d84c393
        public static var secret = "4484e3f6a4fe24c476af705412486d99" //3c35bc147f90c6fc10e0aa5f76e06f46 //4484e3f6a4fe24c476af705412486d99
    }
    
    public struct Sina
    {
        public static var appKey = "3102582006"
        public static var appID = ""
        public static var redirectURI = "https://api.weibo.com/oauth2/default.html"       
    }
    
    public struct ZifuBao{
        public static var scheme = "smoskyapp"
    }
    
    
    public static func initSDKs(sdks:[PopularSDK])
    {
     
        if sdks.contains(.Sina)
        {
            WeiboSDK.enableDebugMode(true)
        
        }
        if sdks.contains(.WX){
            WXApi.registerApp(WX.appID)
            _ = WXCallBack.shared
        }
        if sdks.contains(.QQ){
            _ = QQLoginEngine.shared.auth
        }
    }
}

//
//  MEShareCenter.m
//  培训通
//
//  Created by 朱撷潼 on 14/11/14.
//  Copyright (c) 2014年 zhuxietong. All rights reserved.
//

#import "MEShareCenter.h"
#define imageNum 10
#import "GlobalDefines.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>

#define QQ_APP_ID @"1105802252"
#define WX_APP_ID @"wx3832b2931a1a4c90"
#define WB_APP_ID @"53086094"



NSBundle *shareBundle = nil;

static MEShareCenter *shareCenter = nil;
@interface MEShareCenter()<UIAlertViewDelegate>
@property (nonatomic,strong) NSMutableArray *imagViews;

@end

@implementation MEShareCenter

+ (NSBundle*)shareBundle{
    if (shareBundle == nil)
    {
        return [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"shareInfo" ofType:@"bundle"]];
    }
    else{
        return shareBundle;
    }
}

    
+ (void)setShareBundle:(NSBundle*)bundel{
    shareBundle = bundel;
}

    
+ (UIImage*)imageWith:(NSString*)name{
    return  [UIImage imageNamed:name inBundle:[MEShareCenter shareBundle] compatibleWithTraitCollection:nil];

}


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.tempImageV = [[UIImageView alloc] init];
        self.shareManager = [[ShareManager alloc] init];
        [self addImageViews];
        
    }
    return self;
}

- (void)addImageViews
{
    if (!self.imagViews) {
        self.imagViews = [[NSMutableArray alloc] init];
    }
    for (NSInteger i = 0;i<imageNum;i++) {
        UIImageView *imageView = [[UIImageView alloc] init];
        [self.imagViews addObject:imageView];
    }
    
}

+ (MEShareCenter*)defaultCenter
{
    if (shareCenter==nil) {
        shareCenter = [[self alloc] init];
    }
    return shareCenter;
}

- (void)setShareWebInfo:(NSMutableDictionary *)shareWebInfo
{
    _shareWebInfo = shareWebInfo;
    
    
//    [self.tempImageV sd_setImageWithURL:[NSURL URLWithString:[_shareWebInfo objectForKey:@"img"]]];
//    [self.tempImageV sd_setImageWithURL:[NSURL URLWithString:[_shareWebInfo objectForKey:@"img"]] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        self.defaultImg = image;
//    }];
    
}



- (void)shareWebInfoWithPlatformIndex:(NSInteger)index
{
    
    
    
    if (self.shareWebInfo==nil) {
        NSLog(@"shareWebInfo 信息为空");
        return;
    }
    UIImage *image = self.defaultImg;
    if (!image) {
//        NSLog(@"00-----==dd");
        
        image = [MEShareCenter imageWith:@"share_default"];
    }
    
    
    NSMutableDictionary *shareInfo = [[NSMutableDictionary alloc] init];
    [shareInfo setObject:[_shareWebInfo objectForKey:@"title"] forKey:kShareMessageTitleKey];
    [shareInfo setObject:[_shareWebInfo objectForKey:@"content"] forKey:kShareMessageTextKey];
    [shareInfo setObject:[_shareWebInfo objectForKey:@"url"] forKey:kShareMessageWebpageKey];
    [shareInfo setObject:image forKey:kShareMessageImageKey];

    NSLog(@"share content with index: %ld",(long)index);

    switch (index) {
        case 1:
        {
            
            if ([self  installedShareApp:MESharePaltformWX]) {
                [self.shareManager sendMessageToWeixinWithScene:WeixinSceneTypeSession messageType:ShareMessageTypeWebpage infoDict:shareInfo];
            }
        }
            break;
        case 2:
        {
            if ([self  installedShareApp:MESharePaltformWX]) {
                [self.shareManager sendMessageToWeixinWithScene:WeixinSceneTypeTimeline messageType:ShareMessageTypeWebpage infoDict:shareInfo];
            }
        }
            break;
        case 3:
        {

            if ([self  installedShareApp:MESharePaltformQQ]) {
                [self.shareManager sendMessageToQQWithType:ShareMessageTypeWebpage infoDict:shareInfo];
            }
        }
            break;
        case 4:
        {
            if ([self  installedShareApp:MESharePaltformQQ]) {
                [self.shareManager sendMessageToQQZoneWithType:ShareMessageTypeWebpage infoDict:shareInfo];
            }
        }
            break;
        case 5:
        {
            [self.shareManager sendMessageToWeiboWithType:ShareMessageTypeWebpage infoDict:shareInfo];
        }
            break;
        default:
            break;
    }
  
}

- (void)clearShareInfo
{
    self.tempImageV.image = nil;
    self.shareWebInfo = nil;
}


- (void)loadImageUrls:(NSMutableArray *)array
{
    NSInteger num = 0;
    for (NSString *url in array) {
//        NSLog(@"====-------%@",url);
        if (num<imageNum) {
            UIImageView *imageV = [self.imagViews objectAtIndex:num];
            if (imageV) {
//                imageV.img_url = url;
//                [imageV sd_setImageWithURL:[NSURL URLWithString:url]];
            }
        }
        num++;
    }
}

- (void)setShareCallBackID:(NSString *)shareCallBackID
{
    if (shareCallBackID==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"shareCallBackID"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:shareCallBackID forKey:@"shareCallBackID"];
    }
}



- (NSString*)shareCallBackID
{
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:@"shareCallBackID"];
    
    if (!str) {
        str = @"";
    }
    return str;
}


- (void)tellShareResult:(NSMutableDictionary *)result
{
    if (result) {
        if ([self.delegate respondsToSelector:@selector(tellShareResultInfo:)]) {
            [self.delegate tellShareResultInfo:result];
        }
    }
}



- (BOOL)installedShareApp:(MESharePaltformAPP)paltformAPP
{
    
    if (paltformAPP==MESharePaltformQQ) {
        BOOL install = [QQApiInterface isQQInstalled];
        if (!install) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"未安装手机QQ，是否现在去下载？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"下载", nil];
            [alert setTag:111];
            [alert show];
            
            return NO;
        }

    }
    else if(paltformAPP==MESharePaltformWX)
    {
        BOOL install = [WXApi isWXAppInstalled];
        if (!install) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"未安装微信客户端，是否现在去下载？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"下载", nil];
            [alert setTag:112];
            [alert show];
            return NO;
        }
    }
    else
    {
//        return YES;
//        appStoreUrl = [NSString stringWithFormat:AppStoreInstallURLFormat,WB_APP_ID];

    }
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==111) {
        if (buttonIndex!=0) {
            NSString *appStoreUrl = [NSString stringWithFormat:AppStoreInstallURLFormat,QQ_APP_ID];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreUrl]];
        }
        
    }
    if (alertView.tag==112&&buttonIndex!=0) {
        if (buttonIndex!=0) {
            NSString *appStoreUrl = [NSString stringWithFormat:AppStoreInstallURLFormat,WX_APP_ID];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreUrl]];
      
        }
    }
}


@end




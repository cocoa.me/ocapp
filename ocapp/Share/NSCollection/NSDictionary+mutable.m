//
//  NSDictionary+mutable.m
//  XMLConvert
//
//  Created by WangLei on 15/7/19.
//  Copyright (c) 2015年 zhuxietong. All rights reserved.
//

#import "NSDictionary+mutable.h"
#import "MEMutableFather.h"
@implementation NSDictionary (mutable)

- (NSMutableDictionary*)toMutableDictionary{
    return [MEMutableFather convertDictionary:self];
}


@end

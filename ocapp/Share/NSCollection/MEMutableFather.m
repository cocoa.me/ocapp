//
//  MEMutableFather.m
//  TZMHTeacher
//
//  Created by 朱撷潼 on 14/10/22.
//  Copyright (c) 2014年 zhuxietong. All rights reserved.
//

#import "MEMutableFather.h"

static MEMutableFather *defaultFather = nil;


@implementation MEMutableFather

- (NSMutableArray*)convertToMutableArray:(NSArray *)array
{
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    for (id obj in array) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSMutableDictionary *newDict = [self convertToMutableDictionary:obj];
            [newArray addObject:newDict];
        }
        else
        {
            [newArray addObject:obj];
        }
        
    }
    
    return newArray;
}

- (NSMutableDictionary*)convertToMutableDictionary:(NSDictionary *)dict
{
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    NSArray *keys = [dict allKeys];
    
    for (NSString *key in keys) {
        id obj = [dict objectForKey:key];
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSMutableDictionary *subDict = [self convertToMutableDictionary:obj];
            [newDict setObject:subDict forKey:key];
        }
        else if ([obj isKindOfClass:[NSArray class]])
        {
            [newDict setObject:[self convertToMutableArray:obj] forKey:key];

        }
        else
        {
            [newDict setObject:obj forKey:key];
        }

    }
    
    return newDict;
}



- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

+ (MEMutableFather*)defaultCenter
{
    if (defaultFather==nil) {
        defaultFather = [[self alloc] init];
    }
    return defaultFather;
}




+ (NSMutableArray*)convertArray:(NSArray *)array
{
    return [[MEMutableFather defaultCenter] convertToMutableArray:array];
}


+ (NSMutableDictionary*)convertDictionary:(NSDictionary *)dict
{
    return [[MEMutableFather defaultCenter] convertToMutableDictionary:dict];
}


@end








//
//  NSArray+mutable.m
//  XMLConvert
//
//  Created by WangLei on 15/7/19.
//  Copyright (c) 2015年 zhuxietong. All rights reserved.
//

#import "NSArray+mutable.h"
#import "MEMutableFather.h"

@implementation NSArray (mutable)

- (NSMutableArray*)toMutableArray
{
    return [MEMutableFather convertArray:self];
}


@end

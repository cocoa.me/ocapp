//
//  PayTask.swift
//  Aiwei
//
//  Created by zhu xietong on 2017/11/14.
//  Copyright © 2017年 zhu xietong. All rights reserved.
//

import Foundation
import Eelay




enum PayStatus {
    case waiting
    case success
    case failed
}

enum PayTask {
    case 商品购买
    case 酒店预订
}


public enum SeverPayMethod:String {
    
    case wxPay = "1"
    case aliPay = "2"
    case cardPay = "3"
    
    
    static var defaultMethod:SeverPayMethod = .wxPay
    
    var options:[String:Any]{
        get{
            return ["name":self.name,"icon":self.icon,"id":self.rawValue]
        }
    }
    
    static func method(with ID:String)->PayMethod{
        return PayMethod(rawValue: ID) ?? .defaultMethod
    }
    
    
    
    var name:String{
        get{
            switch self {
            case .aliPay:
                return "支付宝"
            case .wxPay:
                return "微信支付"
            case .cardPay:
                return "银行卡支付"
                
            }
        }
    }
    
    var icon:String{
        switch self {
        case .wxPay:
            return "jd_zf_wx"
        case .aliPay:
            return "jd_zf_zfb"
        default:
            return "jd_zf_yh"
        }
    }
    
    static var payOptions:[[String:Any]]{
        get{
            let o = [
                PayMethod.wxPay.options,
                PayMethod.aliPay.options,
                //                PayMethod.cardPay.options,
            ]
            return o
        }
    }
}


public enum PayMethod:String {
    
    case wxPay = "1"
    case aliPay = "2"
    case cardPay = "3"
    
    
    static var defaultMethod:PayMethod = .wxPay
    
    var options:[String:Any]{
        get{
            return ["name":self.name,"icon":self.icon,"id":self.rawValue]
        }
    }
    
    static func method(with ID:String)->PayMethod{
        return PayMethod(rawValue: ID) ?? .defaultMethod
    }
    
    
    
    var name:String{
        get{
            switch self {
            case .aliPay:
                return "支付宝"
            case .wxPay:
                return "微信支付"
            case .cardPay:
                return "银行卡支付"

            }
        }
    }
    
    var icon:String{
        switch self {
        case .wxPay:
            return "jd_zf_wx"
        case .aliPay:
            return "jd_zf_zfb"
        default:
            return "jd_zf_yh"
        }
    }
    
    static var payOptions:[[String:Any]]{
        get{
            let o = [
                PayMethod.wxPay.options,
                PayMethod.aliPay.options,
//                PayMethod.cardPay.options,
                ]
            return o
        }
    }
}



enum DoPayReuslt
{
    case ok(PayMethod)
    case canle(PayMethod)
    case failed(PayMethod,String)
}


class DoPay {
    
    
    
    static func pay(_ method:PayMethod,pay_id:String,task_type:PayTaskType,_ sign:NSMutableDictionary,finish_block:@escaping (DoPayReuslt,PayTaskType,String)-> Void) {
        switch method {
        case .wxPay:
            FastWXPay.shared.pay(sign, task_type: task_type, finish_block: { (_result, _task_type, reason) in
                switch _result{
                case .ok:
                    finish_block(.ok(.wxPay), _task_type, pay_id)
                case .canle:
                    finish_block(.canle(.wxPay), _task_type, pay_id)
                case .failed:
                    finish_block(.failed(.wxPay,reason), _task_type, pay_id)
                default:
                    finish_block(.failed(.wxPay,reason), _task_type, pay_id)
                }
            })
        case .aliPay:
            FastAliPay.shared.paySigner(sign["sign",""], finish_block: { (_result, msg) in
                switch _result{
                case .ok:
                    finish_block(.ok(.wxPay), task_type, pay_id)
                case .canle:
                    finish_block(.canle(.wxPay), task_type, pay_id)
                case .failed:
                    finish_block(.failed(.wxPay,msg), task_type, pay_id)
                default:
                    finish_block(.failed(.wxPay,msg), task_type, pay_id)
                }
            })
        default:
            break
        }
        
    }

}
